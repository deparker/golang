# Golang

## Introduction

This package holds the spec file and related patches for the Golang package.
The golang package is part of the larger go-toolset meta package.

## Sources

This particular branch provides Go 1.16.x. The sources for this branch can be
found at https://pagure.io/go/tree/go1.16-openssl-fips. The reason the source is
coming from a pagure fork as opposed to an upstream tarball is due to certain
patches we have written and currently maintain in order to claim FIPS compliance
by calling into OpenSSL. Shipping a forked version of the toolchain is not the
ideal scenario, and there is work in progress with upstream to enable us to
instead ship a pure upstream toolchain and include a crypto module in go-toolset
which will satisfy our FIPS requirements.

The current fork is based on an upstream branch[[0]] which uses
boringcrypto[[1]] instead of OpenSSL.

If you need to make changes to the source for a rebase or bug fix, check out the
pagure repo and switch to the branch listed above. Once you have made your
changes you can test them locally with `./all.bash`. You may want to export
`GOLANG_FIPS=1` before running that if you want to verify the FIPS codepaths are
correct. Please note however that the test suite does not fully expect FIPS
compliance, and will attempt to test non FIPS compliant code paths. The easiest
way to test your changes correctly is to create a tarball locally and execute a
mockbuild using this packge, which knows how to correctly run the testsuite in
both FIPS and non-FIPS modes.

NOTE: The way pagure previously handled uploaded releases has changed, and
releases must be tagged in the appropriate branch, from which pagure will
generate source tarballs.

## Testing & building changes

The first test you should run is a local mockbuild. This can be done with the
rhpkg command:

```
rhpkg mockbuild
```

Once everything builds and passes locally you'll likely want to perform a
scratch build. This will ensure that the changes you made build and run
correctly on all architectures that this package supports. The best way to do
this is to run a scratch build from your local sources without first having to
push them. This ensures your changes are correct before commiting them to the
repo. This can also be done via the following rhpkg command:

```
rhpkg scratch-build --srpm
```

Once your scratch build has passed you can execute a real build:

```
rhpkg build
```

---

[0] https://github.com/golang/go/tree/dev.boringcrypto 
[1] https://opensource.google.com/projects/boringssl
